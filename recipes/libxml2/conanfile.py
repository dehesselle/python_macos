# SPDX-FileCopyrightText: 2024 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from conan import ConanFile
from conan.tools.files import get
from conan.tools.gnu import Autotools, AutotoolsToolchain, PkgConfigDeps


class Libxml2Recipe(ConanFile):
    name = "libxml2"
    version = "2.13.5"
    description = "libxml2 for Python framework"
    url = "https://gitlab.com/dehesselle/python_macos"

    settings = "os", "compiler", "build_type", "arch"

    requires = ("python/[>=3.12]",)

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        toolchain = AutotoolsToolchain(self)
        environment = toolchain.environment()
        environment.define(
            "PYTHON",
            "{}/Python.framework/Versions/Current/bin/python3".format(
                self.dependencies["python"].package_folder
            ),
        )
        environment.define(
            "PKG_CONFIG_PATH",
            "{}/Python.framework/Versions/Current/lib/pkgconfig".format(
                self.dependencies["python"].package_folder
            ),
        )
        toolchain.generate(environment)

        pkgconfig = PkgConfigDeps(self)
        pkgconfig.generate()

    def build(self):
        autotools = Autotools(self)
        autotools.configure()
        autotools.make()

    def package(self):
        autotools = Autotools(self)
        autotools.install()
