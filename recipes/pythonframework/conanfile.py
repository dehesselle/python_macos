# SPDX-FileCopyrightText: 2024 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import os
from pathlib import Path

from conan import ConanFile
from conan.tools.files import copy, mkdir, rmdir, replace_in_file

from dylib import Dylib
from framework import Framework


class PythonFrameworkRecipe(ConanFile):
    name = "pythonframework"
    version = "3.12.9"
    description = "relocatable Python.framework bundle"
    url = "https://gitlab.com/dehesselle/python_macos"

    settings = "os", "compiler", "build_type", "arch"

    requires = (
        f"python/{version}",
        "libxml2/[>=2.13]",
    )
    exports = "dylib.py", "framework.py"

    def _package_openssl(self):
        framework = Framework(Path(self.package_folder) / "Python.framework")
        openssl_lib_dir = Path(self.dependencies["openssl"].package_folder) / "lib"
        openssl_ver = self.dependencies["openssl"].ref.version.major

        copy(
            self,
            src=openssl_lib_dir,
            dst=framework.libraries_dir,
            pattern="*.dylib",
        )
        copy(
            self,
            src=openssl_lib_dir,
            dst=framework.libraries_dir,
            pattern=f"engines-{openssl_ver}",
        )
        for lib in framework.libraries_dir.glob(f"engines-{openssl_ver}/*"):
            Dylib(lib).change_dependent_install_names(
                "@loader_path/..", framework.libraries_dir
            )
        copy(
            self,
            src=openssl_lib_dir,
            dst=framework.libraries_dir,
            pattern="ossl-modules",
        )
        for lib in framework.libraries_dir.glob("ossl-modules/*"):
            Dylib(lib).change_dependent_install_names(
                "@loader_path/..", framework.libraries_dir
            )
        Dylib(
            framework.lib_dynload_dir
            / "_ssl.cpython-{v.major}{v.minor}-darwin.so".format(v=self.ref.version)
        ).change_dependent_install_names(
            "@loader_path/../../../Libraries", framework.libraries_dir
        )
        Dylib(
            framework.lib_dynload_dir
            / "_crypt.cpython-{v.major}{v.minor}-darwin.so".format(v=self.ref.version)
        ).change_dependent_install_names(
            "@loader_path/../../../Libraries", framework.libraries_dir
        )

    def _package_libxml2(self):
        framework = Framework(Path(self.package_folder) / "Python.framework")
        libxml2_lib_dir = Path(self.dependencies["libxml2"].package_folder) / "lib"

        copy(
            self,
            src=libxml2_lib_dir,
            dst=framework.libraries_dir,
            pattern="libxml2*",
        )
        copy(
            self,
            src=libxml2_lib_dir,
            dst=framework.lib_dir,
            pattern="python*",
        )
        Dylib(
            framework.site_packages_dir / "libxml2mod.so"
        ).change_dependent_install_names(
            "@loader_path/../../../Libraries", framework.libraries_dir
        )

    def _package_tcl(self):
        framework = Framework(Path(self.package_folder) / "Python.framework")
        tcl_lib_dir = Path(self.dependencies["tcl"].package_folder) / "lib"
        tcl_ver = self.dependencies["tcl"].ref.version.major

        copy(self, src=tcl_lib_dir, dst=framework.lib_dir, pattern="itcl*")
        copy(self, src=tcl_lib_dir, dst=framework.lib_dir, pattern="sqlite*")
        copy(self, src=tcl_lib_dir, dst=framework.lib_dir, pattern=f"tcl{tcl_ver}*")
        copy(self, src=tcl_lib_dir, dst=framework.lib_dir, pattern="tdbc*")
        copy(self, src=tcl_lib_dir, dst=framework.lib_dir, pattern="thread*")

    def _package_tk(self):
        framework = Framework(Path(self.package_folder) / "Python.framework")
        tk_lib_dir = Path(self.dependencies["tk"].package_folder) / "lib"
        tk_ver = self.dependencies["tk"].ref.version.major

        copy(self, src=tk_lib_dir, dst=framework.lib_dir, pattern=f"tk{tk_ver}*")

    def _write_package_folder_to_file(self):
        ci_project_dir = os.environ.get("CI_PROJECT_DIR")
        if ci_project_dir:
            (Path(ci_project_dir) / "package_folder.txt").write_text(
                self.package_folder
            )

    def package(self):
        framework = Framework(Path(self.package_folder) / "Python.framework")

        # copy Python.framework from the python package
        copy(
            self,
            src="{}/Python.framework".format(
                self.dependencies["python"].package_folder
            ),
            dst=framework.dir,
            pattern="*",
        )
        # create Libraries directory for ext. libraries and symlink it
        mkdir(self, framework.libraries_dir)
        os.symlink(
            src="Versions/{v.major}.{v.minor}/Libraries".format(v=self.ref.version),
            dst=framework.dir / "Libraries",
            target_is_directory=True,
        )
        # adjust library link path for Python.app
        Dylib(
            framework.app_dir / "Contents/MacOS/Python"
        ).change_one_dependant_install_name("@executable_path/../../../../Python")
        # link to lib dir for Python.app
        os.symlink(
            src="../../../lib",
            dst=framework.app_dir / "Contents/lib",
            target_is_directory=True,
        )
        # adjust library link path for python3.12 binary
        Dylib(
            framework.bin_dir / "python{v.major}.{v.minor}".format(v=self.ref.version)
        ).change_one_dependant_install_name("@executable_path/../Python")
        # simplify library id
        Dylib(framework.versions_dir / "Python").install_name = "Python"
        # adjust shebangs for Python scripts in bin
        for file in framework.bin_dir.iterdir():
            if (
                not file.is_symlink()
                and file.name != "python{v.major}.{v.minor}".format(v=self.ref.version)
            ):
                replace_in_file(
                    self,
                    file_path=file,
                    search=self.dependencies["python"].package_folder
                    + "/Python.framework/Versions/{v.major}.{v.minor}/bin/python{v.major}.{v.minor}".format(
                        v=self.ref.version
                    ),
                    replace="/usr/bin/env python{v.major}.{v.minor}".format(
                        v=self.ref.version
                    ),
                )
        # remove test package to save space
        rmdir(
            self,
            path=framework.lib_dir
            / "python{v.major}.{v.minor}/test".format(v=self.ref.version),
        )
        # log the package folder to file
        self._write_package_folder_to_file()

        # package dependencies
        self._package_libxml2()
        self._package_openssl()
        self._package_tcl()
        self._package_tk()
