# SPDX-FileCopyrightText: 2024 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from pathlib import Path
import subprocess
import shlex
import re


class Dylib:
    def __init__(self, library: str):
        self.library = library

    def _otool(self, args: str) -> list:
        sp = subprocess.run(
            shlex.split(f"/usr/bin/otool {args} {self.library}"),
            capture_output=True,
            encoding="utf-8",
        )
        return sp.stdout.splitlines()

    def _install_name_tool(self, args: str) -> list:
        sp = subprocess.run(
            shlex.split(f"/usr/bin/install_name_tool {args} {self.library}"),
            capture_output=True,
            encoding="utf-8",
        )
        return sp.stdout.splitlines()

    @property
    def install_name(self) -> str:
        result = self._otool("-D")
        return result[1] if result.count == 2 else ""

    @install_name.setter
    def install_name(self, install_name: str):
        self._install_name_tool(f"-id {install_name}")

    @property
    def depends_on(self) -> list:
        result = list()
        skip_lines = 2 if self.install_name else 1
        for line in self._otool("-L")[skip_lines:]:
            # This matches only dylibs:
            # match = re.match("\t(.+\.dylib)", line)
            # This will match everything:
            match = re.match("\t(.+) \(compatibility", line)
            if match:
                result.append(match.group(1))
        return result

    @property
    def rpath(self) -> list:
        result = list()
        line_iter = iter(self._otool("-l"))
        for line in line_iter:
            if re.match("\s+cmd LC_RPATH", line):
                next(line_iter)
                match = re.match("\s+path (.+) \(offset.+", next(line_iter))
                if match:
                    result.append(match.group(1))
        return result

    def clear_rpath(self):
        for rpath in self.rpath:
            self._install_name_tool(f"-delete_rpath {rpath}")

    def add_rpath(self, rpath: str):
        self._install_name_tool(f"-add_rpath {rpath}")

    def change_one_dependant_install_name(self, install_name: str):
        libs = [x for x in self.depends_on if Path(install_name).name in x]
        if libs:
            self._install_name_tool(f"-change {libs[0]} {install_name}")

    def change_dependent_install_names(self, install_name: str, lib_dir: str):
        libs_in_lib_dir = [
            x for x in Path(lib_dir).glob("*.dylib") if not x.is_symlink()
        ]

        for lib in self.depends_on:
            if Path(lib).name in [Path(x).name for x in libs_in_lib_dir]:
                self._install_name_tool(
                    f"-change {lib} {Path(install_name)/Path(lib).name}"
                )
