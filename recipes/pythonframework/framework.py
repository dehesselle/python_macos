# SPDX-FileCopyrightText: 2024 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from pathlib import Path

from semver import Version


class Framework:
    def __init__(self, path: Path):
        self.dir = path

    @property
    def version(self) -> Version:
        # This is designed for single-version frameworks only.
        return Version.parse(
            next((self.dir / "Versions").iterdir()).name, optional_minor_and_patch=True
        )

    @property
    def versions_dir(self) -> Path:
        return self.dir / "Versions/{v.major}.{v.minor}".format(v=self.version)

    @property
    def libraries_dir(self) -> Path:
        return self.versions_dir / "Libraries"

    @property
    def lib_dir(self) -> Path:
        return self.versions_dir / "lib"

    @property
    def bin_dir(self) -> Path:
        return self.versions_dir / "bin"

    @property
    def site_packages_dir(self) -> Path:
        return self.lib_dir / "python{v.major}.{v.minor}/site-packages".format(
            v=self.version
        )

    @property
    def lib_dynload_dir(self) -> Path:
        return self.lib_dir / "python{v.major}.{v.minor}/lib-dynload".format(
            v=self.version
        )

    @property
    def app_dir(self) -> Path:
        return self.versions_dir / "Resources/Python.app"
