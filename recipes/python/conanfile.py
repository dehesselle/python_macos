# SPDX-FileCopyrightText: 2024 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from conan import ConanFile
from conan.tools.files import get, apply_conandata_patches, export_conandata_patches
from conan.tools.gnu import Autotools, AutotoolsToolchain, PkgConfigDeps


class PythonRecipe(ConanFile):
    name = "python"
    version = "3.12.9"
    description = "Python as macOS framework"
    url = "https://gitlab.com/dehesselle/python_macos"

    settings = "os", "compiler", "build_type", "arch"
    default_options = {
        # The SSL module doesn't support static linking.
        # https://github.com/python/cpython/issues/82975
        # https://github.com/python/cpython/commit/bacefbf41461ab703b8d561f0e3d766427eab367
        "openssl/*:shared": True,
    }

    requires = (
        "gdbm/1.23",
        "gettext/0.22.5",
        "libiconv/1.17",
        "openssl/3.3.2",
        "pkgconf/2.2.0",
        "readline/8.2",
        "tcl/8.6.10",
        "tk/8.6.10",
        "xz_utils/5.4.5",
        "zlib/1.3.1",
    )

    def export_sources(self):
        export_conandata_patches(self)

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)
        apply_conandata_patches(self)

    def generate(self):
        toolchain = AutotoolsToolchain(self, prefix=self.package_folder)
        environment = toolchain.environment()
        environment.define(
            "GDBM_CFLAGS",
            "-I{}/include".format(self.dependencies["gdbm"].package_folder),
        )
        environment.define(
            "GDBM_LIBS",
            "-L{}/lib -lgdbm".format(self.dependencies["gdbm"].package_folder),
        )
        toolchain.configure_args += [
            "--enable-framework={}".format(self.package_folder),
            "--enable-optimizations",
            "--with-openssl-rpath=auto",
            "--with-pkg-config=yes",
        ]
        toolchain.generate(environment)

        pkgconfig = PkgConfigDeps(self)
        pkgconfig.generate()

    def build(self):
        autotools = Autotools(self)
        autotools.configure()
        autotools.make()

    def package(self):
        autotools = Autotools(self)
        autotools.install(
            args=[
                "DESTDIR=",
                f"PYTHONAPPSDIR={self.package_folder}",
                # avoid race condition: can fail when running multi-core
                "-j1",
            ]
        )
