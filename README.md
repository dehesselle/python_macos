# Python for macOS

This repository provides a relocatable `Python.framework` (3.12.x) for macOS, to be used and bundled in other projects.

The minimum supported OS version is macOS Big Sur.

Everything is built from source using [Conan](https://conan.io).

## Special features

Some libraries have been baked-in to provide up-to-date modules for

- `ssl`
- `tkinter`
- `libxml2`

Python has also been patched to

- hard-wire the [pycache prefix](https://docs.python.org/3/whatsnew/3.8.html#parallel-filesystem-cache-for-compiled-bytecode-files) to `~/Library/Caches/com.gitlab.dehesselle.python_macos`
- make the framework fully relocatable

## License

This work is licensed under [GPL-2.0-or-later](LICENSE).  
Python is licensed under [PSF](https://docs.python.org/3/license.html).  
Project icon attribution: [Snake icons created by Smashicons - Flaticon](https://www.flaticon.com/free-icons/snake)
